import {NgModule}             from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

//Guards
import {LoggedInRouteGuard} from './shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from './shared/guards/logged-out-route-guard';
import {TrainerRouteGuard} from './shared/guards/trainer-route-guard';

//Components
import {PageNotFoundComponent} from './core/components/page-not-found/page-not-found.component';
import {AccessDeniedComponent} from './core/components/access-denied/access-denied.component';

//Trainer training plans components
import {DashboardComponent as TrainerDashboardComponent} from './trainer/training-plans/components/dashboard/dashboard.component';
import {ExcersizesListComponent as TrainerExcersizesListComponent} from './trainer/training-plans/components/excersizes-list/excersizes-list.component';
import {ExcersizeFormComponent as TrainerExcersizeFormComponent} from './trainer/training-plans/components/excersize-form/excersize-form.component';

//Trainer client components
import {ClientDashboardComponent as TrainerClientDashboardComponent} from './trainer/client/components/client-dashboard/client-dashboard.component';
import {ClientOverviewComponent as TrainerClientOverviewComponent} from './trainer/client/components/client-overview/client-overview.component';
import {NutritionPlansComponent as TrainerNutritionPlansComponent} from './trainer/client/components/nutrition-plans/nutrition-plans.component';
import {MeasurementLogComponent as TrainerMeasurementLogComponent} from './trainer/client/components/measurement-log/measurement-log.component';
import {InvalidClientComponent as TrainerInvalidClientComponent} from './trainer/client/components/invalid-client/invalid-client.component';
import {WellbeingComponent as TrainerWellbeingComponent} from './trainer/client/components/wellbeing/wellbeing.component';

export const appRoutingProviders: any[] = [];

const routes: Routes = [
    {
        path: 'user',
        loadChildren: './user/user.module#UserModule'
    },
    {
        path: 'messages',
        loadChildren: './messages/messages.module#MessagesModule'
    },
    {
        path: 'trainer/clients',
        loadChildren: './trainer/clients/clients.module#ClientsModule'
    },
    {
        path: 'trainer/training-plans',
        loadChildren: './trainer/training-plans/training-plans.module#TrainingPlansModule'
    },
    {
        path: 'trainer/client',
        loadChildren: './trainer/client/client.module#ClientModule'
    },
//    //TRAINER CLIENT MODULE
//    {
//        path: 'trainer/client/invalid-client',
//        component: TrainerInvalidClientComponent,
//        canActivate: [LoggedInRouteGuard],
//    },
//    {
//        path: 'trainer/client/:cid',
//        component: TrainerClientDashboardComponent,
//        canActivate: [LoggedInRouteGuard, TrainerRouteGuard],
//        children: [
//            {
//                path: '',
//                redirectTo: 'overview',
//                pathMatch: 'full'
//            },
//            {
//                path: 'overview',
//                component: TrainerClientOverviewComponent,
//            },
//            {
//                path: 'training-plans',
//                component: TrainerTrainingTrainingPlansListComponent,
//                pathMatch: 'full',
//            },
//            {
//                path: 'training-plans/create',
//                component: TrainerTrainingPlanFormComponent,
//                pathMatch: 'full',
//            },
//            {
//                path: 'training-plans/update/:id',
//                component: TrainerTrainingPlanFormComponent,
//                pathMatch: 'full',
//            },
//            {
//                path: 'nutrition-plans',
//                component: TrainerNutritionPlansComponent,
//            },
//            {
//                path: 'measurement-log',
//                component: TrainerMeasurementLogComponent,
//            },
//            {
//                path: 'wellbeing',
//                component: TrainerWellbeingComponent,
//            },
//        ]
//    },
    
    {
        path: 'trainer/nutrition-plans',
        loadChildren: './trainer/nutrition-plans/nutrition-plans.module#NutritionPlansModule'
    },
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full' 
    },
    {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
        path: 'access-denied',
        component: AccessDeniedComponent
    },
   {path: '**', component: PageNotFoundComponent}
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [LoggedInRouteGuard, LoggedOutRouteGuard, TrainerRouteGuard]
})

export class AppRoutingModule {}