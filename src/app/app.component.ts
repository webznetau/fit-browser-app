import {Component, ChangeDetectorRef, OnDestroy} from '@angular/core';
import {MediaMatcher} from '@angular/cdk/layout';

//Services
import {UserService} from './user/shared/services/user.service';
import {MessagesService} from './messages/shared/services/messages.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
    mobileQuery: MediaQueryList;
    unreadedMessages: number = 0;
    private unreadedMessagesInterval;
    private _mobileQueryListener: () => void;

    constructor(
        changeDetectorRef: ChangeDetectorRef,
        media: MediaMatcher,
        public userService: UserService,
        private messagesService: MessagesService
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 992px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
        
        if (this.userService.isLogged()) {
            this.getUnreadedMessagesCountDaemon();
        }
    }
    
    private getUnreadedMessagesCountDaemon(): void
    {
        this.getUnreadedMessagesCount();
        
        this.unreadedMessagesInterval = setInterval(()=>{
            this.getUnreadedMessagesCount();
        }, 10000);
    }
    
    private getUnreadedMessagesCount(): void
    {
        this.messagesService.getUnreadedCount().subscribe(
            count => {
                if (count > 0 && (count > this.unreadedMessages)) {
                    this.messagesService.newMessageSound();
                }
                
                this.unreadedMessages = count;               
            },
            errorResponse => {
//                let error = new ErrorResponseHelper(errorResponse, "Can't get plans");   
//                this.alert.error(error.message, error.name);  
            }
        )
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
    }
    
    logout(): void{
        this.userService.logout();
    }

}
