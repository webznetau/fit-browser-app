import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Material
import {MatIconModule, MatToolbarModule, MatSidenavModule, MatListModule, MatButtonToggleModule, MatButtonModule, MatBadgeModule, MatMenuModule} from '@angular/material';
import {MatTabsModule, MatInputModule, MatProgressSpinnerModule, MatProgressBarModule, MatTableModule, MatCardModule, MatExpansionModule, MatAutocompleteModule, MatTooltipModule, MatSelectModule} from '@angular/material';

//Shared module
import {SharedModule} from './shared/shared.module';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);

//Components
import {AppComponent} from './app.component';
import {PageNotFoundComponent} from './core/components/page-not-found/page-not-found.component';
import {NotificationsComponent} from './core/components/notifications/notifications.component';
import {AccessDeniedComponent} from './core/components/access-denied/access-denied.component';


@NgModule({
  declarations: [
    //Ng
    AppComponent,
    //App
    PageNotFoundComponent,
    NotificationsComponent,
    AccessDeniedComponent
  ],
  imports: [
    //Shared module
    SharedModule,
    //NG
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    //Material
    MatIconModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatBadgeModule,
    MatMenuModule,
    MatTabsModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatSelectModule,
  ],
  providers: [

  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
