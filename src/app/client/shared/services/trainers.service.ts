import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {Trainer} from '../interfaces/trainer';

//Services
import {AppSettingsService} from '../../../shared/services/app-settings.service';
import {UserService} from '../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class TrainersService {

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
    
    getTrainers(): Observable<Trainer[]>{
        return this.http.get<Trainer[]>(`${this.appSettings.get('apiUrl')}/clients/trainers${this.userService.getCredentialsQueryString()}`);
    }
    
}
