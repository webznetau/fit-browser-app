//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Guards
import {LoggedInRouteGuard} from '../shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from '../shared/guards/logged-out-route-guard';

//Components
import {DashboardComponent} from './components/dashboard/dashboard.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: DashboardComponent,
        canActivate: [LoggedInRouteGuard]
   },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [LoggedInRouteGuard, LoggedOutRouteGuard]
})
export class DashboardRoutingModule {}
