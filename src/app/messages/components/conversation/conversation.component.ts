//NG
import {Component, OnInit, OnDestroy, EventEmitter, Output, ElementRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

//Interfaces
import {Message} from '../../interfaces/messages';
import {ConversationDetails} from '../../interfaces/messages';

//Services
import {MessagesService} from '../../shared/services/messages.service';
import {AlertService} from '../../../shared/services/alert.service';
import {UserService} from '../../../user/shared/services/user.service';

//Helpers
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {
    @Output() seen: EventEmitter<any> = new EventEmitter();
    @ViewChild('messagesList') messagesList: ElementRef;
    
    id: number;
    conversation: Message[];
    conversationDetails: ConversationDetails = {
        title: null
    };
    toId: number;
    interval;

    constructor(
        private messagesService: MessagesService,
        private alert: AlertService,
        private userService: UserService,
        private route: ActivatedRoute,
    ) {}

    ngOnInit() {
        this.route.params.subscribe(() => {
            this.id = +this.route.snapshot.paramMap.get('id');
            clearInterval(this.interval);            
            this.conversationDaemon();
        });      
    }
    
    ngOnDestroy()
    {
        clearInterval(this.interval);
    }
    
    private conversationDaemon(): void
    {
        this.getConversation(true);

//        this.interval = setInterval(() => {
//            this.getConversation();
//        }, 10000);
    }
    
    getConversation(clear: boolean = false): void
    {
        if (clear) {
            this.conversation = null;
        }
        
        this.messagesService.getConversation(this.id).subscribe(
            messages => {
                this.conversation = messages;
                this.seen.emit(true);
                
                if (!this.issetConversationDetails()) {
                    this.setConversationDetails(messages);
                }
                
                setTimeout(() => {
                    this.scrollToEnd();
                }, 500);
                
                for (let message of messages) {
                    if (+message.from_id === this.userService.getId() ) {
                        this.toId = message.to_id;
                        break;
                    }
                    
                    if (+message.to_id === this.userService.getId()) {
                        this.toId = message.from_id;
                        break;
                    }
                }
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get conversation");   
                this.alert.error(error.message, error.name);  
            }
        )
    }
    
    private setConversationDetails(messages: Message[]): void
    {
        this.conversationDetails.title = this.isOwnMessage(messages[0]) ? messages[0].to_name : messages[0].from_name
    }
    
    private issetConversationDetails(): boolean
    {
        return !this.conversationDetails.title ? false : true;
    }
    
    isOwnMessage(conversation: Message): boolean
    {
        return conversation.from_id == this.userService.getId() ? true : false;
    }
    
    onReply(message: Message)
    {        
        this.conversation.unshift(message);
        
        setTimeout(() => {
            this.scrollToEnd();
        }, 500);
    }
    
    private scrollToEnd(): void
    {
        if (typeof this.messagesList !== 'undefined') {
            this.messagesList.nativeElement.scrollTop = this.messagesList.nativeElement.scrollHeight;
        }
    }

}
