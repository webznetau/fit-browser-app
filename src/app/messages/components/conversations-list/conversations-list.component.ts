//Ng
import {Component, OnInit, OnDestroy} from '@angular/core';

//Interfaces
import {Message} from '../../interfaces/messages';

//Services
import {MessagesService} from '../../shared/services/messages.service';
import {AlertService} from '../../../shared/services/alert.service';
import {UserService} from '../../../user/shared/services/user.service';

//Helpers
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
    selector: 'app-conversations-list',
    templateUrl: './conversations-list.component.html',
    styleUrls: ['./conversations-list.component.css']
})
export class ConversationsListComponent implements OnInit 
{

    conversations: Message[];
    interval: any;
    seenIds = [];

    constructor(
        private messagesService: MessagesService,
        private alert: AlertService,
        private userService: UserService
    ) { }

    ngOnInit() 
    {
        this.conversationsDaemon();
    }
    
    ngOnDestroy()
    {
        clearInterval(this.interval);
    }
    
    private conversationsDaemon(): void
    {
        this.getConversations();
        
        this.interval = setInterval(() => {
            this.seenIds = [];
            this.getConversations();
        }, 10000);
    }
    
    private getConversations(): void
    {
        this.messagesService.getConversations().subscribe(
            conversations => {
                this.conversations = conversations;
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get plans");   
                this.alert.error(error.message, error.name);  
            }
        )
    }
    
    isOwnMessage(conversation: Message): boolean
    {
        return conversation.from_id == this.userService.getId() ? true : false;
    }
    
    isNewMessage(conversation: Message): boolean
    {
        return !conversation.seen_time && !this.isOwnMessage(conversation)? true : false;
    }

}
