//NG
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';

//Services
import {MessagesService} from '../../shared/services/messages.service';
import {AlertService} from '../../../shared/services/alert.service';
import {UserService} from '../../../user/shared/services/user.service';
import {ClientsService} from '../../../trainer/clients/services/clients.service';
import {TrainersService} from '../../../client/shared/services/trainers.service';

//Interfaces
import {Message} from '../../interfaces/messages';
import {Trainer} from '../../../client/shared/interfaces/trainer';
import {Client} from '../../../trainer/clients/models/client';

//Helpers
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-create-message',
  templateUrl: './create-message.component.html',
  styleUrls: ['./create-message.component.css']
})
export class CreateMessageComponent implements OnInit {

    clients = [
        {'id': 5, 'name': 'Tony montana'},
        {'id': 4, 'name': 'Podopieczny Grześka'}, 
        {'id': 3, 'name': 'danielo ploskonka'},
    ];
    
    recipients: Trainer[] | Client[];

    constructor(
        private messagesService: MessagesService,
        private alert: AlertService,
        private userService: UserService,
        private clientsService: ClientsService,
        private trainersService: TrainersService
    ) { }

    ngOnInit() {
        this.getRecipients();
    }

    messageForm : FormGroup = new FormGroup({        
        message: new FormControl('', [
            Validators.required
        ]),
        recipients: new FormControl('', [
            Validators.required
        ])
    } );
    
    getRecipients(): void
    {
        if (this.userService.isTypeTrainer()) {
            this.clientsService.getAll().subscribe(
                clients => {
                    this.recipients = clients;
                    console.log(clients);
                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't get recipients");   
                    this.alert.error(error.message, error.name);                     
                }
            );
        }
        
        if (this.userService.isTypeClient()) {
             this.trainersService.getTrainers().subscribe(
                trainers => {
                    this.recipients = trainers;
                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't get recipients");   
                    this.alert.error(error.message, error.name);                     
                }
            );
        }
    }
    
    send(): void 
    {        
        if (this.messageForm.valid) {           
            let content = this.messageForm.controls.message.value;
            
            let message: Message = {
                to_id: this.messageForm.controls.recipients.value,
                from_id: this.userService.getId(),
                content: content
            };
            
            this.messagesService.sendMessage(message).subscribe(
                () => {
                    this.messageForm.setValue({message: null, recipients: null});
                    this.messageForm.markAsPristine();
                    this.messageForm.markAsUntouched();
                    this.messageForm.updateValueAndValidity();
                    this.alert.success("Message sent!");   

                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't send message.");   
                    this.alert.error(error.message, error.name);  
                }
            )
        }
    }

}
