//NG
import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';

//Services
import {MessagesService} from '../../shared/services/messages.service';
import {AlertService} from '../../../shared/services/alert.service';
import {UserService} from '../../../user/shared/services/user.service';

//Interfaces
import {Message} from '../../interfaces/messages';

//Helpers
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-reply',
  templateUrl: './reply.component.html',
  styleUrls: ['./reply.component.css']
})
export class ReplyComponent implements OnInit {
    @Output() newMessage: EventEmitter<any> = new EventEmitter();
    @Input() toId: number;
    @Input() conversationId: number;
    messageForm: FormGroup;

    constructor(
        private messagesService: MessagesService,
        private alert: AlertService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.generateForm();
    }
    
    generateForm(): void
    {
        this.messageForm = new FormGroup({        
            message: new FormControl('', [
                //Validators.required
            ])
        });
    }
    
    reply(): void 
    {
        if (this.messageForm.valid) {           
            let content = this.messageForm.controls.message.value;
            
            let message: Message = {
                to_id: this.toId,
                from_id: this.userService.getId(),
                conversation_id: this.conversationId,
                content: content
            };
            
            this.messagesService.sendMessage(message).subscribe(
                () => {
                    this.messageForm.setValue({message: null});
                    this.messageForm.markAsPristine();
                    this.messageForm.markAsUntouched();
                    this.messageForm.updateValueAndValidity();

                    this.newMessage.emit(message);
                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't get conversation");   
                    this.alert.error(error.message, error.name);  
                }
            )
        }
    }

}
