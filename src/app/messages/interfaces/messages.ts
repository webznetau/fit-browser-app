export interface Message
{
    id?: number;
    conversation_id?: number;
    to_id: number;
    from_id: number;
    content: string;
    time?: number;
    seen_time?: number;
    from_name?: string;
    to_name?: string;
}

export interface ConversationDetails
{
    title: string;
}