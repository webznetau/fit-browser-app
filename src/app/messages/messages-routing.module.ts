import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {MessagesDashboardComponent} from './components/messages-dashboard/messages-dashboard.component';
import {CreateMessageComponent} from './components/create-message/create-message.component';
import {ConversationComponent} from './components/conversation/conversation.component';

//Guards
import {LoggedInRouteGuard} from '../shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from '../shared/guards/logged-out-route-guard';

const routes: Routes = [
    {
        path: '',
        component: MessagesDashboardComponent,
        canActivate: [LoggedInRouteGuard],
        children: [
            {
                path: '',
                component: CreateMessageComponent

            },
            {
                path: 'conversation/:id',
                component: ConversationComponent,
            }
        ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MessagesRoutingModule { }
