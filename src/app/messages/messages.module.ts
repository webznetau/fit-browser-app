import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MessagesRoutingModule} from './messages-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Components
import {MessagesDashboardComponent} from './components/messages-dashboard/messages-dashboard.component';
import {ConversationsListComponent} from './components/conversations-list/conversations-list.component';
import {CreateMessageComponent} from './components/create-message/create-message.component';
import {ConversationComponent} from './components/conversation/conversation.component';

//Material
import {MatButtonModule, MatProgressSpinnerModule, MatProgressBarModule, MatTooltipModule, MatInputModule, MatIconModule, MatOptionModule, MatSelectModule} from '@angular/material';
import { ReplyComponent } from './components/reply/reply.component';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);


@NgModule({
    imports: [
        CommonModule,
        MessagesRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        FontAwesomeModule,
        //Material
        MatInputModule,
        MatIconModule,
        MatButtonModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatTooltipModule,
        MatOptionModule,
        MatSelectModule
    ],
    declarations: [
        MessagesDashboardComponent,
        ConversationsListComponent,
        CreateMessageComponent,
        ConversationComponent,
        ReplyComponent
    ]
})
export class MessagesModule {}