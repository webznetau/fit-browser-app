import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {Message} from '../../interfaces/messages';

//Services
import {AppSettingsService} from '../../../shared/services/app-settings.service';
import {UserService} from '../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class MessagesService 
{

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
    
    getConversations(): Observable<Message[]>
    {
        return this.http.get<Message[]>(`${this.appSettings.get('apiUrl')}/messages/conversations${this.userService.getCredentialsQueryString()}`);
    }
    
    getConversation(id: number): Observable<Message[]>
    {
        return this.http.get<Message[]>(`${this.appSettings.get('apiUrl')}/messages/conversations/${id}${this.userService.getCredentialsQueryString()}`);
    }
    
    sendMessage(message: Message): Observable<Message>
    {
        return this.http.post<Message>(`${this.appSettings.get('apiUrl')}/messages/create${this.userService.getCredentialsQueryString()}`, message, httpOptions);     
    }
    
    getUnreadedCount(): Observable<number>
    {
        return this.http.get<number>(`${this.appSettings.get('apiUrl')}/messages/getunreadedcount${this.userService.getCredentialsQueryString()}`);
    }
    
    newMessageSound(): void
    {
        let audio = new Audio();
        audio.src = "../../../assets/message.mp3";
        audio.load();
        audio.play();
    }
    
}
