import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionPlansListComponent } from './nutrition-plans-list.component';

describe('NutritionPlansListComponent', () => {
  let component: NutritionPlansListComponent;
  let fixture: ComponentFixture<NutritionPlansListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutritionPlansListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionPlansListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
