import {Component, OnInit, OnDestroy} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Location} from '@angular/common';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatOptionSelectionChange} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {DragulaService} from 'ng2-dragula';

//Model
import {TrainingPlan, TrainingGroup, TrainingExcersize, Excersize} from '../../../trainer/training-plans/shared/interfaces/training-plan';

//Services
import {AlertService} from '../../services/alert.service';
import {ExcersizesService} from '../../../trainer/training-plans/shared/services/excersizes.service';
import {PlansService} from '../../../trainer/training-plans/shared/services/plans.service';

//Helpers
import {FormsHelper} from '../../helpers/forms.helper';
import {ErrorResponseHelper} from '../../helpers/error-response.helper';

@Component({
    selector: 'app-plan-form',
    templateUrl: './plan-form.component.html',
    styleUrls: ['./plan-form.component.css'],
    viewProviders: [DragulaService]
})
export class TrainingPlanFormComponent implements OnInit, OnDestroy {

    //Forms
    form: FormGroup;
    groupForm: FormGroup;
    excersizeForms: FormGroup[] = [];
    
    //Other
    plan: TrainingPlan = {
        name: '',
        groups: []
    };
    groups: TrainingGroup[];
    excersizes: TrainingExcersize[];
    mode: string;
    updatingGroup: number;
    updatingGroupHtmlElement: HTMLElement;
    updatingExcersize: number[] = [];
    updatingExcersizeHtmlElement: HTMLElement;
    inProgress: boolean;
    formReady: boolean;
    exportedExcersize: string;
    client: number;
        
    //Excersizes templates
    excersizesTemplates: Excersize[] = []; 
    excersizesTemplatesNames: any = []; //Array of excersizes name to check if excersize template exist
    filteredExcersizesTemplates: Observable<Excersize[]>;
    filteredExcersizesControl = new FormControl();

    constructor(
        private location: Location,
        private alert: AlertService,
        private excersizesService: ExcersizesService,
        private plansService: PlansService,
        private router: Router,
        private route: ActivatedRoute,
        private dragulaService: DragulaService
    ) {
        dragulaService.createGroup("GROUPS", {
            moves: (el, container, handle) => {
              return handle.className === 'handle';
            }
        });
    }

    ngOnDestroy(){
        this.dragulaService.destroy("GROUPS");
    }

    ngOnInit() {                  
        this.client = +this.route.parent.snapshot.paramMap.get('cid');
         
        this.getExcersizesTemplates();
        this.generateForms();
        
        this.filteredExcersizesTemplates = this.filteredExcersizesControl.valueChanges
        .pipe(
            startWith<string | Excersize>(''),
            map(value => typeof value === 'string' ? value : value.name),
            map(name => name ? this._filterExcersizesTemplates(name) : this.excersizesTemplates.slice())
        );
    }
    
    generateForms(): void{      
        this.generateForm();
        this.generateGroupForm();
    }
    
    getPlan(id: number): void{
        this.plansService.get(id).subscribe(         
            plan => {
                delete(plan.created_at);
                delete(plan.updated_at);
                delete(plan.tid);
                delete(plan.cid);
                
                this.prepareUpdateData(plan);   
            },
            errorResponse => {                           
                let error = new ErrorResponseHelper(errorResponse, "Can't get plan data");                
                this.alert.error(error.message, error.name);
                this.formReady = false;
            }
        );
    }
    
    private prepareUpdateData(plan: TrainingPlan): void{
        this.form.setValue(plan);  
        this.plan.groups = JSON.parse(plan.groups); 
        
        if(this.plan.groups.length > 0){
            for(var _i = 0; _i < this.plan.groups.length; _i++){
                this.excersizeForms[_i] = this.generateExcersizeForm();
            }
        }       
                        
        this.formReady = true;
    }
    
    addGroup(group: TrainingGroup): void{
        if(this.groupForm.valid){
            group.excersizes = [];
            this.alert.success("Group " + group.name + " added!");      
            FormsHelper.resetForm(this.groupForm);
            this.excersizeForms.push(this.generateExcersizeForm());
            this.plan.groups.push(group); 
        }
    }    
        
    addExcersize(group: TrainingGroup, excersize: TrainingExcersize): void{
        group.excersizes.push(excersize);
        let group_i = this.plan.groups.indexOf(group, 0);
        FormsHelper.resetForm(this.excersizeForms[group_i]);
        this.alert.success("Excersize " + group.name + " added!");
    }
    
    groupFormCancel(): void{
        if(this.updatingGroup){
            this.updatingGroup = null;
            this.scrollTo(this.updatingGroupHtmlElement);
        }
        FormsHelper.resetForm(this.groupForm);
    }
    
    excersizeFormCancel(gi: number): void{
        if(this.updatingExcersize[gi]){
            this.updatingExcersize[gi] = null;
            this.scrollTo(this.updatingExcersizeHtmlElement);
        }
        FormsHelper.resetForm(this.excersizeForms[gi]);
    }
    
    generateForm(): void{        
        let id = +this.route.snapshot.paramMap.get('id');
        this.mode = id ? 'update' : 'create';
        
        this.form = new FormGroup({
            id: new FormControl(),
            groups: new FormControl(),
            name: new FormControl('', [
                Validators.required,
                Validators.minLength(3)
            ]),
            desc: new FormControl('', [])
        });        
                
        switch(this.mode) {            
            case 'update':                                    
                this.getPlan(id);             
            break;            
            case 'create':                                      
                this.formReady = true;                
            break;
        }  
    }
    
    generateGroupForm(): void{
        this.groupForm = new FormGroup({
            name: new FormControl('', [
                Validators.required,
                Validators.minLength(3)
            ]),
            desc: new FormControl('', []),
            excersizes: new FormControl('')
        });
    }
    
    generateExcersizeForm(): FormGroup{
        return new FormGroup({
            name: new FormControl('', [
                Validators.required,
                Validators.minLength(3)
            ]),
            desc: new FormControl('', []),            
            h_links: new FormControl('', []),
            weight: new FormControl('', []),
            sets: new FormControl('', []),
            reps: new FormControl('', []),
            rest: new FormControl('', []),
            tempo: new FormControl('', []),
            duration: new FormControl('', []),
            comment: new FormControl('', []),       
        });
    }
    
    setUpdatingGroup(id: number, data: TrainingGroup, groupHtmlHandler: HTMLElement){
        this.updatingGroup = id + 1;
        this.groupForm.setValue(data);
        this.updatingGroupHtmlElement = groupHtmlHandler;
    }
    
    setUpdatingExcersize(id: number, gi: number, data:TrainingExcersize, excersizeHtmlHandler: HTMLElement){
        this.updatingExcersize[gi] = id + 1;
        this.excersizeForms[gi].setValue(data);
        this.updatingExcersizeHtmlElement = excersizeHtmlHandler;
    }
    
    updateGroup(data: TrainingGroup){
        this.plan.groups[this.updatingGroup - 1] = data;
        this.updatingGroup = null;
        FormsHelper.resetForm(this.groupForm);
        this.scrollTo(this.updatingGroupHtmlElement);
    }
    
    updateExcersize(data: TrainingExcersize, gi: number){
        this.plan.groups[gi].excersizes[this.updatingExcersize[gi]-1]= data;
        this.updatingExcersize[gi] = null;
        FormsHelper.resetForm(this.excersizeForms[gi]);
        this.scrollTo(this.updatingExcersizeHtmlElement);
    }
    
    deleteGroup(group: TrainingGroup){
        if(confirm("Do you really want to remove excersize " + group.name + '?')){
            let index = this.plan.groups.indexOf(group, 0);
            if (index > -1) {
                this.plan.groups.splice(index, 1);
            }
        }
    }
    
    getExcersizesTemplates(): void{
        this.excersizesService.getAll().subscribe(
            excersizes => {
                this.excersizesTemplates = excersizes;
                
                for(let excersize of excersizes){
                    this.excersizesTemplatesNames.push(excersize.name.toLowerCase());
                }
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get excersizes templates");   
                this.alert.error(error.message, error.name);                     
            }
        );
    }
    
    private _filterExcersizesTemplates(value: string): Excersize[] {
        const filterValue = value.toLowerCase();

        return this.excersizesTemplates.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
    }
    
    setExcersizeDataFromTemplate(event: MatOptionSelectionChange, excersize: Excersize, form: FormGroup): void{
        if (event.source.selected) {
            form.controls.name.setValue(excersize.name);
            form.controls.desc.setValue(excersize.desc);
            form.controls.h_links.setValue(excersize.h_links);
        }
    }
    
    deleteExcersize(excersize: TrainingExcersize, gi: number){
        if(confirm("Do you really want to remove excersize " + excersize.name + '?')){
            let index = this.plan.groups[gi].excersizes.indexOf(excersize, 0);
            if (index > -1) {
                this.plan.groups[gi].excersizes.splice(index, 1);
            }
        }
    }
    
    goBack(): void {        
        this.location.back();        
    }
    
    splitHelpfulLinks(links: string): any{
        return links.split('\n');
    }
    
    scrollTo(element: HTMLElement) {
        element.scrollIntoView({behavior: 'smooth'});
    }   
   
    submit(){
        if(this.form.valid){
            if(this.mode == 'create'){
                this.create();
            }
            else if(this.mode == 'update'){
                this.update();
            }
        }
    }
    
    create(): void{
        this.inProgress = true;
    
        let data: TrainingPlan = this.form.value;
        
        data.groups = this.plan.groups;
        
        this.plansService.create(data, this.client).subscribe(         
            () => {                               
                this.inProgress = false;
                this.alert.success("Plan created!"); 
                this.router.navigate(this.client ? ['/trainer/client/'+this.client+'/training-plans'] : ['/trainer/training-plans']);                
            },
            errorResponse => {                
                this.inProgress = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't create plan");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    update(): void{
        this.inProgress = true;
    
        let data: TrainingPlan = this.form.value;
        
        data.groups = this.plan.groups;
        
        this.plansService.update(data, this.client).subscribe(         
            () => {                               
                this.inProgress = false;
                this.alert.success("Plan updated!");           
            },
            errorResponse => {                
                this.inProgress = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't update plan");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    exportExcersizeTemplate(excersize: TrainingExcersize, gi: number, ei: number): void{
        let data: Excersize = {
            name: excersize.name,
            desc: excersize.desc,
            h_links: excersize.h_links
        };
        
        this.exportedExcersize = excersize.name;
        
        this.excersizesService.create(data).subscribe(         
            () => {                               
                this.exportedExcersize = null;
                this.alert.success("Excersize exported!");
                this.excersizesTemplatesNames.push(excersize.name.toLowerCase());
                this.getExcersizesTemplates();
            },
            errorResponse => {                
                this.inProgress = false;  
                this.exportedExcersize = null;              
                let error = new ErrorResponseHelper(errorResponse, "Can't export excersize.");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    excersizeTemplateIsExported(excersize: Excersize): boolean{        
        return (this.excersizesTemplatesNames.indexOf(excersize.name.toLowerCase()) > -1) ? true : false;
    }
    
    moveExcersize(direction: string, gi:number,  ei: number){        
        if(direction == 'down'){
            this.swapExcersizes(this.plan.groups[0].excersizes, ei, ei+1);
        }
        else if(direction == 'up'){
            this.swapExcersizes(this.plan.groups[0].excersizes, ei, ei-1);
        }
    }
    
    private swapExcersizes(data: TrainingExcersize[], ia, ib) {
        var temp = data[ia];

        data[ia] = data[ib];
        data[ib] = temp;
    }
    

}
