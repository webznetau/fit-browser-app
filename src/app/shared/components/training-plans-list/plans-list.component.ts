import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {MatOptionSelectionChange} from '@angular/material';

//Services
import {AlertService} from '../../services/alert.service';
import {PlansService} from '../../../trainer/training-plans/shared/services/plans.service';

//Helpers
import {ErrorResponseHelper} from '../../helpers/error-response.helper';

//Models
import {TrainingPlan} from '../../../trainer/training-plans/shared/interfaces/training-plan';

@Component({
  selector: 'app-plans-list',
  templateUrl: './plans-list.component.html',
  styleUrls: ['./plans-list.component.css']
})
export class TrainingPlansListComponent implements OnInit {

    plans: TrainingPlan[];
    
    templates: TrainingPlan[] = [];
    filteredTemplates: Observable<TrainingPlan[]>;
    filteredTemplatesControl = new FormControl();
    selectedTemplateToAdd: TrainingPlan;
    
    displayedColumns: string[] = ['name', 'created', 'last_modified' ,'actions']; 
    dataSource: any;   
    copied: number;
    deleting: number;
    client: number;

    constructor(
        private alert: AlertService,
        private plansService: PlansService,
        private router: Router,
        private route: ActivatedRoute,
    ) { }

    ngOnInit() {
        let _client = this.route.parent.snapshot.paramMap.get('id');
        this.client = +this.route.parent.snapshot.paramMap.get('id');
        
        if(_client && _client.length > 0 && !this.client){
            this.router.navigate(['trainer/client/invalid-client']);
            throw new Error('Missing Client Param');
        }

        if(this.client){           
            this.getTemplates();
        }
        
        this.getAll();
    }
    
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    
    setTemplateToAdd(event: MatOptionSelectionChange, template: TrainingPlan){
        if(event.source.selected) {
            this.selectedTemplateToAdd = template;
        }
    }
    
    addFromTemplate(): void{
        this.copy(this.selectedTemplateToAdd);
        this.selectedTemplateToAdd = null;
    }
    
    getAll(): void{ 
              
        this.plansService.getAll(this.client).subscribe(
            plans => {
                this.plans = plans;
                this.dataSource = new MatTableDataSource(plans);
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get plans");   
                this.alert.error(error.message, error.name);                     
            }
        );
        
        this.dataSource = new MatTableDataSource(this.plans);
    }
    
    getTemplates(): void{ 
              
        this.plansService.getAll(0).subscribe(
            plans => {
                this.templates = plans;                
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get plans templates");   
                this.alert.error(error.message, error.name);                     
            }
        );
    }
    
    copy(plan: TrainingPlan): void{         
        this.copied = plan.id;
        
        this.plansService.copy(plan.id, this.client).subscribe(
            () => {
                this.alert.success(plan.name + ' copied!');
                this.getAll();
                this.copied = null;
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't copy plan");   
                this.alert.error(error.message, error.name);    
                this.copied = null;                 
            }
        );
    }
    
    remove(plan: TrainingPlan): void{
        if(confirm("Do you really want to remove plan " + plan.name)){
            this.deleting = plan.id;
            
            this.plansService.remove(plan.id).subscribe(
                () => {
                    this.alert.success(plan.name + ' deleted!');
                    this.getAll();
                    this.deleting = null;
                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't delete plan");   
                    this.alert.error(error.message, error.name);    
                    this.deleting = null;                 
                }
            );
        }
    }

    getUpdateLink(planId: number): string{
        if(this.client){
            return './update/' + planId;
        }
        return '../update/' + planId;
    }

}
