export class ErrorResponseHelper { 
      
    name : string ;
    message : string;
    prependName : string;
    prependMessage : string;
    
    constructor( errorResponse : any, prependMessage : string = '', prependName : string = '' ) {        
        this.prependMessage = prependMessage;        
        this.prependName = prependName;        
        
        return this.parse( errorResponse );        
   }
    
    private parse(errorResponse: any){        
        if(errorResponse.hasOwnProperty('error')){                    
            this.name = errorResponse.error.hasOwnProperty('name') ? (this.prependName ?  this.prependName + ' : ' : '') + errorResponse.error.name : this.prependName;
            this.message = errorResponse.error.hasOwnProperty('message') ? (this.prependMessage ?  this.prependMessage + ' : ' : '') + errorResponse.error.message : this.prependMessage;            
        }
        else{            
            this.name = '';
            this.message = '';           
        }
        
        return this;        
    }
    
}
