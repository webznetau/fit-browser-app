import {FormControl, FormGroup, Validators} from '@angular/forms';

export class FormsHelper{
    
    static resetForm(formGroup: FormGroup) {
        let control = null;
        formGroup.reset();
        formGroup.markAsUntouched();
        
        Object.keys(formGroup.controls).forEach((name) => {
            control = formGroup.controls[name];
            control.setErrors(null);
        });
        
        formGroup.updateValueAndValidity();
    }
    
}