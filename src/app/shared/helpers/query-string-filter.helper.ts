import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class QueryStringFilterHelper { 
    
    prepare( filters : any ) : any {     
                
        let queryString = '';
        
        for( let param in filters ) {
            
            for( let operator in filters[ param ] ) {

                if( filters[param][operator] ) {
                    
                    queryString += '&filters[' + param + '][' + operator + ']=' + filters[ param ][ operator ];
                    
               }
                
           }
            
       }
        
        return queryString;
        
   }
    
}
