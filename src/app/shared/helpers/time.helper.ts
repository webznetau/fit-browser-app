import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class TimeHelper { 
    
    todayBeginTimestamp( seconds: boolean = true ): any {
        let d = new Date();   
        let timestamp = (new Date(d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate() + ' 0:00')).getTime();     
        
        return seconds ? Math.floor( timestamp / 1000 ): timestamp;
   }
    
    tomorrowBeginTimestamp( seconds: boolean = true ): any {
        let plus = seconds ? 86400 : 8640000;
        return this.todayBeginTimestamp( seconds ) + plus;
   }
    
    tomorrowEndTimestamp( seconds: boolean = true ): any {
        let plus = seconds ? 86400 : 8640000;
        return this.tomorrowBeginTimestamp( seconds ) + plus;
   }
    
    yesterdayBeginTimestamp( seconds: boolean = true ): any {
        let minus = seconds ? 86400 : 8640000;
        return this.todayBeginTimestamp( seconds ) - minus;
   }
    
    yesterdayEndTimestamp( seconds: boolean = true ): any {
        let minus = seconds ? 1 : 1000;
        return this.todayBeginTimestamp( seconds ) - minus;
   }
    
    todayEndTimestamp( seconds: boolean = true ): any {
        let plus = seconds ? 86400 - 1 : 8640000 - 1000;        
        return this.todayBeginTimestamp( seconds ) + plus;
   }
    
    nowTimestamp( seconds: boolean = true ): any {
        let timestamp =  (new Date()).getTime();
        
        return seconds ? Math.floor( timestamp / 1000 ): timestamp;
   }
    
}