export class ApiHttpErrorResponse{
    error: ApiHttpErrorResponseData
}

export class ApiHttpErrorResponseData{
    name: string;
    message: string;
    code: boolean;
    status: number;
    type: string;
}

export class ApiInfoResponse{
    message?: string;
    id?: number
}