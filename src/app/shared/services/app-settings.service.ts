import {Injectable} from '@angular/core';
//import {Observable , of} from 'rxjs';

import {AppSettings} from '../appsettings';

@Injectable({
  providedIn: 'root'
})
export class AppSettingsService {

    constructor( 
        
    ) {}
    
    get( name : string ): any {
        let settings = new AppSettings();
        return settings[name] ? settings[name] : null;
   }
    
}
