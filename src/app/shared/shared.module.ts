import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DragulaModule} from "ng2-dragula"

//Material
import {MatInputModule, MatAutocompleteModule, MatExpansionModule, MatButtonModule, MatCardModule, MatIconModule, MatSelectModule, MatTableModule, MatProgressSpinnerModule, MatProgressBarModule} from '@angular/material';

//Components
import {TrainingPlansListComponent} from './components/training-plans-list/plans-list.component';
import {TrainingPlanFormComponent} from './components/training-plan-form/plan-form.component';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NutritionPlansListComponent} from './components/nutrition-plans-list/nutrition-plans-list.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FontAwesomeModule,
        FormsModule, 
        ReactiveFormsModule,
        DragulaModule,
        //Material
        MatInputModule,
        MatIconModule,
        MatSelectModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatProgressBarModule,
        MatCardModule,
        MatButtonModule,
        MatExpansionModule,
        MatAutocompleteModule
    ],
    declarations: [
        TrainingPlansListComponent,
        TrainingPlanFormComponent,
        NutritionPlansListComponent
    ],
    exports: [
        TrainingPlansListComponent,
        TrainingPlanFormComponent,
        NutritionPlansListComponent
    ]
})
  
export class SharedModule {}