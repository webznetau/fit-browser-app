import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//Guards
import {LoggedInRouteGuard} from '../../shared/guards/logged-in-route-guard';
import {TrainerRouteGuard} from '../../shared/guards/trainer-route-guard';

//Components
import {ClientDashboardComponent} from './components/client-dashboard/client-dashboard.component';
import {ClientOverviewComponent} from './components/client-overview/client-overview.component';
import {TrainingPlansListComponent} from '../../shared/components/training-plans-list/plans-list.component';
import {TrainingPlanFormComponent} from '../../shared/components/training-plan-form/plan-form.component';
import {NutritionPlansComponent} from './components/nutrition-plans/nutrition-plans.component';
import {MeasurementLogComponent} from './components/measurement-log/measurement-log.component';
import {InvalidClientComponent} from './components/invalid-client/invalid-client.component';
import {WellbeingComponent} from './components/wellbeing/wellbeing.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/client/invalidclient',
        canActivate: [LoggedInRouteGuard],
        pathMatch: 'full' 
    },
    {
        path: 'invalidclient',
        component: InvalidClientComponent,
        canActivate: [LoggedInRouteGuard],
    },
    {
        path: ':id',
        component: ClientDashboardComponent,
        canActivate: [LoggedInRouteGuard, TrainerRouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'overview',
                pathMatch: 'full'
            },
            {
                path: 'overview',
                component: ClientOverviewComponent,
            },
            {
                path: 'training-plans',
                component: TrainingPlansListComponent,
            },
            {
                path: 'training-plans/create',
                component: TrainingPlanFormComponent
            },
            {
                path: 'training-plans/update/:id',
                component: TrainingPlanFormComponent
            },
            {
                path: 'nutrition-plans',
                component: NutritionPlansComponent,
            },
            {
                path: 'measurementlog',
                component: MeasurementLogComponent,
            },
            {
                path: 'wellbeing',
                component: WellbeingComponent,
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }