import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientRoutingModule} from './client-routing.module';
import {RouterModule} from '@angular/router';

import {SharedModule} from '../../shared/shared.module';

//Components
import {ClientDashboardComponent} from './components/client-dashboard/client-dashboard.component';
import {InvalidClientComponent} from './components/invalid-client/invalid-client.component';
import {ClientOverviewComponent} from './components/client-overview/client-overview.component';
import {NutritionPlansComponent} from './components/nutrition-plans/nutrition-plans.component';
import {MeasurementLogComponent} from './components/measurement-log/measurement-log.component';
import {WellbeingComponent} from './components/wellbeing/wellbeing.component';


//Material
import {MatTabsModule, MatInputModule, MatIconModule, MatProgressSpinnerModule, MatProgressBarModule, MatButtonModule, MatTableModule, MatCardModule} from '@angular/material';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);

@NgModule({
  imports: [
    ClientRoutingModule,
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    SharedModule,
    //Material
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule
  ],
  declarations: [ClientDashboardComponent, InvalidClientComponent, ClientOverviewComponent, NutritionPlansComponent, MeasurementLogComponent, WellbeingComponent]
})
export class ClientModule { }
