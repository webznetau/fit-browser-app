import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvalidClientComponent } from './invalid-client.component';

describe('InvalidClientComponent', () => {
  let component: InvalidClientComponent;
  let fixture: ComponentFixture<InvalidClientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvalidClientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvalidClientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
