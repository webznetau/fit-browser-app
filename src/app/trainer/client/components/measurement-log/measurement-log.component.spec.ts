import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementLogComponent } from './measurement-log.component';

describe('MeasurementLogComponent', () => {
  let component: MeasurementLogComponent;
  let fixture: ComponentFixture<MeasurementLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementLogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasurementLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
