import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NutritionPlansComponent } from './nutrition-plans.component';

describe('NutritionPlansComponent', () => {
  let component: NutritionPlansComponent;
  let fixture: ComponentFixture<NutritionPlansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NutritionPlansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NutritionPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
