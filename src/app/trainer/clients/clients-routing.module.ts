//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {ClientsDashboardComponent} from './components/clients-dashboard/clients-dashboard.component';
import {ClientsListComponent} from './components/clients-list/clients-list.component';
import {ClientsInvitationsComponent} from './components/clients-invitations/clients-invitations.component';

//Guards
import {LoggedInRouteGuard} from '../../shared/guards/logged-in-route-guard';
import {TrainerRouteGuard} from '../../shared/guards/trainer-route-guard';

const routes: Routes = [
    {
        path: '',
        component: ClientsDashboardComponent,
        canActivate: [LoggedInRouteGuard, TrainerRouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: ClientsListComponent,
            },
            {
                path: 'invitations',
                component: ClientsInvitationsComponent,
            },
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
