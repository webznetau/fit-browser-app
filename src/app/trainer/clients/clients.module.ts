import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ClientsRoutingModule} from './clients-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);

//Componetns
import {ClientsListComponent} from './components/clients-list/clients-list.component';
import {ClientsDashboardComponent} from './components/clients-dashboard/clients-dashboard.component';
import {ClientsInvitationsComponent} from './components/clients-invitations/clients-invitations.component';

//Material
import {MatTabsModule, MatInputModule, MatTooltipModule, MatIconModule, MatProgressSpinnerModule, MatProgressBarModule, MatButtonModule, MatTableModule, MatCardModule} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    ClientsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    //Material
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule,
    MatTooltipModule
  ],
  declarations: [ClientsListComponent, ClientsDashboardComponent, ClientsInvitationsComponent]
})
export class ClientsModule {}
