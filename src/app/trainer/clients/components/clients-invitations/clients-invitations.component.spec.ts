import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientsInvitationsComponent } from './clients-invitations.component';

describe('ClientsInvitationsComponent', () => {
  let component: ClientsInvitationsComponent;
  let fixture: ComponentFixture<ClientsInvitationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientsInvitationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientsInvitationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
