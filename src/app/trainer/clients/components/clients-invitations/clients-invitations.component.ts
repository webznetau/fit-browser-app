import {Component, OnInit, OnDestroy} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {FormControl, Validators} from '@angular/forms';

//Services
import {AlertService} from '../../../../shared/services/alert.service';
import {ClientsInvitationsService} from '../../services/clients-invitations.service';
import {UserService} from '../../../../user/shared/services/user.service';

//Helpers
import {ErrorResponseHelper} from '../../../../shared/helpers/error-response.helper';

//Models
import {Invitation} from '../../models/invitation';

@Component({
  selector: 'app-clients-invitations',
  templateUrl: './clients-invitations.component.html',
  styleUrls: ['./clients-invitations.component.css']
})
export class ClientsInvitationsComponent implements OnInit, OnDestroy {

    invitations: Invitation[];
    dataSource: any;
    getAllInterval: any;
    
    sendingAgainId: number;
    cancelingId: number;
    sendingInvitation: boolean;
    invitationEmail: FormControl = new FormControl('', [
        Validators.email,
        Validators.required
    ]);
    
    displayedColumns: string[] = ['email', 'send_time', 'status', 'send_again', 'cancel'];

    constructor(
        private alert: AlertService,
        private clientsInvitationsService: ClientsInvitationsService,
        private userService: UserService
    ) { }

    ngOnInit() {
        this.getAll();
        this.getAllInterval = setInterval(()=>{
            this.getAll();
        }, 30000);
    }
    
    ngOnDestroy() {
        clearInterval(this.getAllInterval);
    }
    
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    
    create(): void{
        this.sendingInvitation = true;
        
        let invitationData: Invitation = {
            uid: this.userService.credentials.uid,
            email: this.invitationEmail.value
        };
        
        this.clientsInvitationsService.create(invitationData).subscribe(         
            () => {                               
                this.sendingInvitation = false;
                this.alert.success("Invitation sent");   
                this.sendingInvitation = false;
                this.invitationEmail.reset();       
                this.getAll();
            },
            errorResponse => {                
                this.sendingInvitation = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't send invitation");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    getAll(): void{
        this.clientsInvitationsService.getAll().subscribe(
            invitations => {
                this.invitations = invitations;
                this.dataSource = new MatTableDataSource(invitations);
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get invitations");   
                this.alert.error(error.message, error.name);                     
            }
        );
    }
    
    statusBadge(status: string): string{
        let badge = '';
        
        switch(status){
            case '1':
                badge = 'badge-success';
                break;
             case '-1':
                badge = 'badge-danger';
                break;
            default:
            case '0':
                badge = 'badge-info';
                break;
        }
        return badge;
    }
    
    statusBadgeText(status: string): string{
        let text = '';
        
        switch(status){
            case '1':
                text = 'Accepted';
                break;
             case '-1':
                text = 'Canceled';
                break;
            default:
            case '0':
                text = 'Pending';
                break;
        }
        return text;
    }

    
    sendAgain(id: number): void{
        this.sendingAgainId = id;
        
        this.clientsInvitationsService.sendAgain(id).subscribe(         
            () => {                               
                this.alert.success("Invitation sent");     
                this.getAll();
                this.sendingAgainId = null;
            },
            errorResponse => {           
                let error = new ErrorResponseHelper(errorResponse, "Can't send again invitation");                
                this.alert.error(error.message, error.name);
                this.sendingAgainId = null;
            }
        ); 
    }
    
    cancel(invitation: Invitation): void{
        this.cancelingId = invitation.id;
        
        this.clientsInvitationsService.cancel(invitation.id).subscribe(         
            () => {                               
                this.getAll();
                this.alert.success("Invitation canceled");      
                this.cancelingId = null;
            },
            errorResponse => {           
                let error = new ErrorResponseHelper(errorResponse, "Can't send again invitation");                
                this.alert.error(error.message, error.name);
                this.cancelingId = null;
            }
        ); 
    }


}
