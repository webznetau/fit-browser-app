import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';

//Services
import {AlertService} from '../../../../shared/services/alert.service';
import {ClientsService} from '../../services/clients.service';

//Helpers
import {ErrorResponseHelper} from '../../../../shared/helpers/error-response.helper';

//Models
import {Client} from '../../models/client';

@Component({
  selector: 'app-clients-list',
  templateUrl: './clients-list.component.html',
  styleUrls: ['./clients-list.component.css']
})
export class ClientsListComponent implements OnInit {

    clients: Client[];
    dataSource;    
    displayedColumns: string[] = ['first_name', 'last_name', 'email', 'message', 'manage'];

    constructor(
        private alert: AlertService,
        private clientsService: ClientsService
    ) { }

    ngOnInit() {
        this.getAll();
    }    
    
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }
    
    getAll(): void {
        this.clientsService.getAll().subscribe(
            clients => {
                this.clients = clients;
                this.dataSource = new MatTableDataSource(clients);
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get clients");   
                this.alert.error(error.message, error.name);                     
            }
        );
    }
}
