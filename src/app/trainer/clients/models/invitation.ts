export interface Invitation{
    uid: number;
    email: string;
    id?: number;
    created_at?: number;
    updated_at?: number;
    status?: number;
    token?: string;
}