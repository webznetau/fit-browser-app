import { TestBed, inject } from '@angular/core/testing';

import { ClientsInvitationsService } from './clients-invitations.service';

describe('ClientsInvitationsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ClientsInvitationsService]
    });
  });

  it('should be created', inject([ClientsInvitationsService], (service: ClientsInvitationsService) => {
    expect(service).toBeTruthy();
  }));
});
