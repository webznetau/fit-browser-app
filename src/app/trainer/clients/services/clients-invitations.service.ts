import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {Invitation} from '../models/invitation';

//Services
import {AppSettingsService} from '../../../shared/services/app-settings.service';
import {UserService} from '../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ClientsInvitationsService {

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
  
    create(data: Invitation): Observable<Invitation>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/invitations${this.userService.getCredentialsQueryString()}`, data, httpOptions);        
    }
    
    sendAgain(id: number): Observable<any>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/invitations/send-again${this.userService.getCredentialsQueryString()}&id=${id}`, {}, httpOptions);        
    }
    
    cancel(id: number): Observable<any>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/invitations/cancel${this.userService.getCredentialsQueryString()}&id=${id}`, {}, httpOptions);        
    }
    
    getAll(): Observable<Invitation[]>{
        return this.http.get<Invitation[]>(`${this.appSettings.get('apiUrl')}/invitations${this.userService.getCredentialsQueryString()}`);
    }
    
}
