import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {Client} from '../models/client';

//Services
import {AppSettingsService} from '../../../shared/services/app-settings.service';
import {UserService} from '../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ClientsService {

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
    
    getAll(): Observable<Client[]>{
        return this.http.get<Client[]>(`${this.appSettings.get('apiUrl')}/clients${this.userService.getCredentialsQueryString()}`);
    }
    
}
