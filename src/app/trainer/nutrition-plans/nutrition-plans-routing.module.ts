//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {NutritionPlansListComponent} from '../../shared/components/nutrition-plans-list/nutrition-plans-list.component';
import {MealsListComponent} from './components/meals-list/meals-list.component';
import {ProductsListComponent} from './components/products-list/products-list.component';

//Guards
import {LoggedInRouteGuard} from '../../shared/guards/logged-in-route-guard';
import {TrainerRouteGuard} from '../../shared/guards/trainer-route-guard';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [LoggedInRouteGuard, TrainerRouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'list'
            },
            {
                path: 'list',
                component: NutritionPlansListComponent,
            },
            {
                path: 'meals',
                component: MealsListComponent
            },
            {
                path: 'products',
                component: ProductsListComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NutritionPlansRoutingModule { }
