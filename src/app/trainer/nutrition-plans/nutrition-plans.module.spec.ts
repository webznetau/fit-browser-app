import { NutritionPlansModule } from './nutrition-plans.module';

describe('NutritionPlansModule', () => {
  let nutritionPlansModule: NutritionPlansModule;

  beforeEach(() => {
    nutritionPlansModule = new NutritionPlansModule();
  });

  it('should create an instance', () => {
    expect(nutritionPlansModule).toBeTruthy();
  });
});
