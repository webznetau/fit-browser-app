import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NutritionPlansRoutingModule} from './nutrition-plans-routing.module';

//Shared module
import {SharedModule} from '../../shared/shared.module';

//Components
import {DashboardComponent } from './components/dashboard/dashboard.component';
import {MealsListComponent } from './components/meals-list/meals-list.component';
import {ProductsListComponent} from './components/products-list/products-list.component';

//Material
import {MatTabsModule, MatInputModule, MatIconModule, MatProgressSpinnerModule, MatProgressBarModule, MatButtonModule, MatTableModule, MatCardModule} from '@angular/material';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    NutritionPlansRoutingModule,
    FontAwesomeModule,
    //Material
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule
  ],
  declarations: [MealsListComponent, DashboardComponent, DashboardComponent, ProductsListComponent]
})
export class NutritionPlansModule { }
