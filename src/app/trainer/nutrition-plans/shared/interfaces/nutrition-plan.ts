/**
 * Nutrition Plan Structure
 * @property tid Trainer ID (plans are always assigned to a trainer)
 * @property cid = Client ID (sometimes plan is assigned to a concrete client
 */
export interface NutritionPlan{
    id?: number;
    tid?: number;
    cid?: number;
    name: string;
    desc?: string;
    groups?: NutritionPlanGroup[];
    created_at?: number;
    updated_at?: number;
}

/**
 * Day of the week, low carb day, cheat day etc.
 */
export interface NutritionPlanGroup{
    id?: number;
    name: string;
    desc?: string;
    meals?: NutritionPlanMeal[];
}

/**
 * Breakfast, dinner etc.
 * @property tid Trainer ID
 */
export interface NutritionPlanMeal{
    id?: number;
    tid?: number;
    name: string;
    desc?: string;
    hour?: string;
    products: NutritionPlanProduct[];
}

/**
 * To describe
 * @property tid Trainer ID
 */
export interface NutritionPlanProduct{
    id?: number;
    tid?: number;
    name: string;
    weight: number;
    nutritionalValues?: NutritionalValues;
}

/**
 * To describe
 */
export interface NutritionalValues{
    calories: number;
    proteins: number;
    carbs: NutritionalValuesCarbs;
    fats: NutritionalValuesFats;
}

/**
 * To describe
 */
export interface NutritionalValuesCarbs{
    overall: number;
    simple?: number;
}

/**
 * To describe
 */
export interface NutritionalValuesFats{
    overall: number;
    saturated?: number;
    unsaturated?: number;
    trans?: number;
}