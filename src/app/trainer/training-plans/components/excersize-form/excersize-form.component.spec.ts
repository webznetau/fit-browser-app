import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcersizeFormComponent } from './excersize-form.component';

describe('ExcersizeFormComponent', () => {
  let component: ExcersizeFormComponent;
  let fixture: ComponentFixture<ExcersizeFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcersizeFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcersizeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
