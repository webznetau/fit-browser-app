import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {Location} from '@angular/common';

//Services
import {AlertService} from '../../../../shared/services/alert.service';
import {ExcersizesService} from '../../shared/services/excersizes.service';

//Helpers
import {ErrorResponseHelper} from '../../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-excersize-form',
  templateUrl: './excersize-form.component.html',
  styleUrls: ['./excersize-form.component.css']
})
export class ExcersizeFormComponent implements OnInit {

    form: FormGroup;
    mode: string;
    inProgress: boolean;
    submitText: string;
    formReady: boolean;
    
    constructor(
        private alert: AlertService,
        private excersizesService: ExcersizesService,
        private router: Router,
        private route: ActivatedRoute,
        private location: Location
    ) { }

    ngOnInit() {
        this.generateForm();
    }
    
    generateForm(): void{                
        let id = +this.route.snapshot.paramMap.get('id');
        this.mode = id ? 'update' : 'create';
        
        this.form = new FormGroup({
            id: new FormControl(''),
            name: new FormControl('', [
                Validators.required,
                Validators.minLength(3)
            ]),
            desc: new FormControl(''),            
            h_links: new FormControl('')
        });
        
        switch(this.mode) {            
            case 'update':                                    
                this.getExcersize(id);                 
            break;            
            case 'create':                                      
                this.formReady = true;                
            break;
        }     
    }
    
    getExcersize(id: number): void{
        this.excersizesService.get(id).subscribe(         
            excersize => {
                this.form.setValue(excersize);
                this.formReady = true;
            },
            errorResponse => {                           
                let error = new ErrorResponseHelper(errorResponse, "Can't get excersize data");                
                this.alert.error(error.message, error.name);
                this.formReady = false;
            }
        );
    }
    
    create(): void{
        this.inProgress = true;
        
        this.excersizesService.create(this.form.value).subscribe(         
            () => {                               
                this.inProgress = false;
                this.alert.success("Excersize created!"); 
                this.router.navigate(['/trainer/training-plans/excersizes/']);                
            },
            errorResponse => {                
                this.inProgress = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't create excersize");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    update(): void{
        this.inProgress = true;
        
        this.excersizesService.update(this.form.value).subscribe(         
            () => {                               
                this.inProgress = false;
                this.alert.success("Excersize updated!");                
            },
            errorResponse => {                
                this.inProgress = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't create excersize");                
                this.alert.error(error.message, error.name);
            }
        );
    }
    
    submit(): void {                              
        this.mode == 'update' ? this.update() : this.create();                               
    }
    
    goBack(): void {        
        this.location.back();        
    }

}
