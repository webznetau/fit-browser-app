import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcersizesListComponent } from './excersizes-list.component';

describe('ExcersizesListComponent', () => {
  let component: ExcersizesListComponent;
  let fixture: ComponentFixture<ExcersizesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExcersizesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcersizesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
