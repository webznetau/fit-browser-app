import {Component, OnInit} from '@angular/core';

import {MatTableDataSource} from '@angular/material';

//Services
import {AlertService} from '../../../../shared/services/alert.service';
import {ExcersizesService} from '../../shared/services/excersizes.service';

//Helpers
import {ErrorResponseHelper} from '../../../../shared/helpers/error-response.helper';

//Models
import {Excersize} from '../../shared/interfaces/training-plan';

@Component({
  selector: 'app-excersizes-list',
  templateUrl: './excersizes-list.component.html',
  styleUrls: ['./excersizes-list.component.css']
})
export class ExcersizesListComponent implements OnInit {

    excersizes: Excersize[];
    displayedColumns: string[] = ['name', 'actions']; 
    dataSource: any;   
    removingId: number;

    constructor(
        private alert: AlertService,
        private excersizesService: ExcersizesService
    ) { }

    ngOnInit() {
        this.getAll();
    }

    getAll(): void{
        this.excersizesService.getAll().subscribe(
            excersizes => {
                this.excersizes = excersizes;
                this.dataSource = new MatTableDataSource(excersizes);
            },
            errorResponse => {
                let error = new ErrorResponseHelper(errorResponse, "Can't get excersizes");   
                this.alert.error(error.message, error.name);                     
            }
        );
        
        this.dataSource = new MatTableDataSource(this.excersizes);
    }
    
    remove(excersize: Excersize){
        if(window.confirm('Are sure you want to delete this excersize?')){
            this.removingId = excersize.id;
            this.excersizesService.remove(excersize.id).subscribe(
                () => {
                    this.excersizes.splice(this.excersizes.indexOf(excersize), 1);
                    this.dataSource = new MatTableDataSource(this.excersizes);
                    this.alert.success("Excersize deleted!");
                    this.removingId = null;
                },
                errorResponse => {
                    let error = new ErrorResponseHelper(errorResponse, "Can't delete excersize");
                    this.alert.error(error.message, error.name); 
                    this.removingId = null;
                }
            );            
        };
    }
    
    applyFilter(filterValue: string) {
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

}
