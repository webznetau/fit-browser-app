export interface TrainingPlan{
    id?: number;
    tid?: number;
    cid?: number;
    name: string;
    desc?: string;
    groups?: TrainingGroup[] | any;
    created_at?: number;
    updated_at?: number;
}

export interface TrainingGroup{
    id?: number;
    name: string;
    desc?: string;
    excersizes?: TrainingExcersize[];
}

export class TrainingExcersize implements Excersize{
    id?: number;
    name: string;
    desc?: string;
    h_links?: string;
    weight?: number;
    sets?: number;
    reps?: string;
    rest?: string;
    tempo?: string;
    comment?: string;
}

export interface Excersize{
    id?: number;
    name: string;
    desc?: string;
    h_links?: string;
}