import { TestBed, inject } from '@angular/core/testing';

import { ExcersizesService } from './excersizes.service';

describe('ExcersizesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExcersizesService]
    });
  });

  it('should be created', inject([ExcersizesService], (service: ExcersizesService) => {
    expect(service).toBeTruthy();
  }));
});
