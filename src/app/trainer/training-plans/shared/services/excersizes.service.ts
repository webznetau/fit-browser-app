import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {Excersize} from '../interfaces/training-plan';

//Services
import {AppSettingsService} from '../../../../shared/services/app-settings.service';
import {UserService} from '../../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
  providedIn: 'root'
})
export class ExcersizesService {

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
    
    create(data: Excersize): Observable<Excersize>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/excersizes${this.userService.getCredentialsQueryString()}`, data, httpOptions);        
    }
    
    update(data: Excersize): Observable<Excersize>{        
        return this.http.put<any>(`${this.appSettings.get('apiUrl')}/excersizes/${data.id}${this.userService.getCredentialsQueryString()}`, data, httpOptions);        
    }
    
    getAll(): Observable<Excersize[]>{
        return this.http.get<Excersize[]>(`${this.appSettings.get('apiUrl')}/excersizes${this.userService.getCredentialsQueryString()}`);
    }
    
    remove(id: number): Observable<any> {              
        return this.http.delete<any>( `${this.appSettings.get( 'apiUrl' )}/excersizes/${id}${this.userService.getCredentialsQueryString()}`, httpOptions );        
    }
    
    get(id: number): Observable<Excersize>{
        return this.http.get<Excersize>(`${this.appSettings.get('apiUrl')}/excersizes/${id}${this.userService.getCredentialsQueryString()}`);
    }
    
}
