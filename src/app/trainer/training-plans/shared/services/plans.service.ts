import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Models
import {TrainingPlan} from '../interfaces/training-plan';

//Services
import {AppSettingsService} from '../../../../shared/services/app-settings.service';
import {UserService} from '../../../../user/shared/services/user.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

@Injectable({
  providedIn: 'root'
})
export class PlansService {

    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService,
        private userService: UserService
    ) { }
    
    create(data: TrainingPlan, cid: number): Observable<TrainingPlan>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/plans${this.userService.getCredentialsQueryString()}&cid=${cid}`, data, httpOptions);        
    }
    
    update(data: TrainingPlan, cid: number): Observable<TrainingPlan>{        
        return this.http.put<any>(`${this.appSettings.get('apiUrl')}/plans/${data.id}${this.userService.getCredentialsQueryString()}&cid=${cid}`, data, httpOptions);        
    }
    
    getAll(cid: number): Observable<TrainingPlan[]>{
        return this.http.get<TrainingPlan[]>(`${this.appSettings.get('apiUrl')}/plans${this.userService.getCredentialsQueryString()}&cid=${cid}`);
    }
    
    remove(id: number): Observable<any> {              
        return this.http.delete<any>( `${this.appSettings.get( 'apiUrl' )}/plans/${id}${this.userService.getCredentialsQueryString()}`, httpOptions );        
    }
    
    get(id: number): Observable<TrainingPlan>{
        return this.http.get<TrainingPlan>(`${this.appSettings.get('apiUrl')}/plans/${id}${this.userService.getCredentialsQueryString()}`);
    }
    
    copy(id: number, cid: number): Observable<TrainingPlan>{        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/plans/copy/${id}${this.userService.getCredentialsQueryString()}&cid=${cid}`, [], httpOptions);        
    }
    
}
