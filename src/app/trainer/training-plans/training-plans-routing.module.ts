//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {TrainingPlanFormComponent} from '../../shared/components/training-plan-form/plan-form.component';
import {ExcersizesListComponent} from './components/excersizes-list/excersizes-list.component';
import {ExcersizeFormComponent} from './components/excersize-form/excersize-form.component';
import {TrainingPlansListComponent} from '../../shared/components/training-plans-list/plans-list.component';

//Guards
import {LoggedInRouteGuard} from '../../shared/guards/logged-in-route-guard';
import {TrainerRouteGuard} from '../../shared/guards/trainer-route-guard';

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [LoggedInRouteGuard, TrainerRouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            },
            {
                path: 'list',
                component: TrainingPlansListComponent,
            },
            {
                path: 'create',
                component: TrainingPlanFormComponent,
            },
            {
                path: 'update/:id',
                component: TrainingPlanFormComponent,
            },
            {
                path: 'excersizes',
                component: ExcersizesListComponent
            },
            {
                path: 'excersizes/create',
                component: ExcersizeFormComponent
            },
            {
                path: 'excersizes/update/:id',
                component: ExcersizeFormComponent
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TrainingPlansRoutingModule { }