import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TrainingPlansRoutingModule} from './training-plans-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {DragulaModule} from 'ng2-dragula';

//Shared module
import {SharedModule} from '../../shared/shared.module';

//Components
import {ExcersizesListComponent} from './components/excersizes-list/excersizes-list.component';
import {ExcersizeFormComponent} from './components/excersize-form/excersize-form.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';

//Material
import {MatTabsModule, MatSelectModule, MatInputModule, MatIconModule, MatProgressSpinnerModule, MatProgressBarModule, MatButtonModule, MatTableModule, MatCardModule, MatExpansionModule, MatAutocompleteModule, MatTooltipModule} from '@angular/material';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/pro-solid-svg-icons';
library.add(fas);

@NgModule({
  imports: [
    TrainingPlansRoutingModule,
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,  
    RouterModule,
    DragulaModule.forRoot(),
    //Material
    MatTabsModule,
    MatInputModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatTableModule,
    MatCardModule,
    MatProgressBarModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatTooltipModule,
    MatSelectModule,
    SharedModule
  ],
  declarations: [ExcersizesListComponent, ExcersizeFormComponent, DashboardComponent]
})
export class TrainingPlansModule {}
