import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {UserGuestScreenComponent} from './user-guest-screen.component';

describe('UserGuestScreenComponent', () => {
  let component: UserGuestScreenComponent;
  let fixture: ComponentFixture<UserGuestScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGuestScreenComponent ]
   })
    .compileComponents();
 }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGuestScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
 });

  it('should create', () => {
    expect(component).toBeTruthy();
 });
});
