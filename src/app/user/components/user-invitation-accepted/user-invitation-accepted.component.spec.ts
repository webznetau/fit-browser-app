import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInvitationAcceptedComponent } from './user-invitation-accepted.component';

describe('UserInvitationAcceptedComponent', () => {
  let component: UserInvitationAcceptedComponent;
  let fixture: ComponentFixture<UserInvitationAcceptedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInvitationAcceptedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInvitationAcceptedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
