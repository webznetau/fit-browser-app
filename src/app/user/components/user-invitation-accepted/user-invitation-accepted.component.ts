import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-invitation-accepted',
  templateUrl: './user-invitation-accepted.component.html',
  styleUrls: ['./user-invitation-accepted.component.css']
})
export class UserInvitationAcceptedComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
