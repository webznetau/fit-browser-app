import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserInvitationFailComponent } from './user-invitation-fail.component';

describe('UserInvitationFailComponent', () => {
  let component: UserInvitationFailComponent;
  let fixture: ComponentFixture<UserInvitationFailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserInvitationFailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserInvitationFailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
