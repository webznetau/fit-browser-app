import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-invitation-fail',
  templateUrl: './user-invitation-fail.component.html',
  styleUrls: ['./user-invitation-fail.component.css']
})
export class UserInvitationFailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
