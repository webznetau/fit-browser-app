//Ng
import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators } from '@angular/forms';

//Services
import {UserService} from '../../shared/services/user.service';
import {AlertService} from '../../../shared/services/alert.service';
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

    inProgress : boolean = false;

    constructor( 
        private userService: UserService,
        private alert: AlertService,
        private router: Router,
        private location: Location
    ) { }

    ngOnInit() {
    }   
    
    loginForm : FormGroup = new FormGroup({
        
        email: new FormControl( '', [
            Validators.required,
            Validators.email
        ]),

        password: new FormControl( '', [
            Validators.required
        ]),
        
        remember_me: new FormControl( false, [])

    } );
    
    login() : void {
        
        this.inProgress = true;
        
        this.userService.getAuthData( this.loginForm.value ).subscribe(        
        
            userCredentials => {                             
                
                this.userService.setCredentials( userCredentials, this.loginForm.value.remember_me );
                
                //this.alert.success( `Nice to see You ${this.loginForm.value.username} ` );
                
                this.inProgress = false;
                                
                this.userService.getData().subscribe(

                    userData => {
                        
                        this.userService.setData( userData );
                        
                        this.router.navigate(['/']);
                        
                    },
                    
                    errorResponse => { 
                        
                        this.alert.error( errorResponse.error.message, errorResponse.error.name );
                        
                        this.userService.logout();
                        
                    }

                );
                
            },
            
            errorResponse => { 
                
                let error = new ErrorResponseHelper( errorResponse, '', "Can't login" );
                
                this.alert.error( error.message, error.name );
                
                this.inProgress = false;
                
            }
            
        );
    }

}
