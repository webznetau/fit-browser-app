import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

//Services
import {UserService} from '../../shared/services/user.service';
import {AlertService} from '../../../shared/services/alert.service';

//Helpers
import {ErrorResponseHelper} from '../../../shared/helpers/error-response.helper';

@Component({
  selector: 'app-user-signup',
  templateUrl: './user-signup.component.html',
  styleUrls: ['./user-signup.component.css']
})
export class UserSignupComponent implements OnInit {
    
    signupForm: FormGroup;
    
    inProgress: boolean = false;
    invitation: boolean = false;

    constructor(
        private userService: UserService,
        private alert: AlertService,
        private router: Router,
        private route: ActivatedRoute
    ) { }

    ngOnInit() { 
        this.generateForm();
    }
    
    generateForm(): void{        
        let inv_token = this.route.snapshot.queryParams['inv_token'];
        let email = this.route.snapshot.queryParams['email'];
      
        if(inv_token && email){
            this.invitation = true;
        }
        
        this.signupForm = new FormGroup({
            first_name: new FormControl('', [
                Validators.required
            ]),

            last_name: new FormControl('', [
                Validators.required
            ]),

            email: new FormControl(email ? email : '', [
                Validators.required,
                Validators.email
            ]),

            password: new FormControl('', [
                Validators.required,
                Validators.minLength(8)
            ]),

            password_repeat: new FormControl('', [
                Validators.required,
                Validators.minLength(8)
            ]),

            inv_token: new FormControl(inv_token ? inv_token : null)

        });
        
        
    }
    
    signup() : void {        
        this.inProgress = true;
        
        this.userService.create( this.signupForm.value ).subscribe(         
            () => {                               
                this.inProgress = false;
                this.alert.success("Account Created! Now you can log in!");
                this.router.navigate(['/user/guest/login']);                
            },
            errorResponse => {                
                this.inProgress = false;                
                let error = new ErrorResponseHelper(errorResponse, "Can't Signup");                
                this.alert.error(error.message, error.name);
            }
        );
        
    }
    
    passwordsMatch() : void {       
        
        let password = this.signupForm.get('password');
        let password_repeat = this.signupForm.get('password_repeat');
         
        let match = ( !password.value || !password_repeat.value ) || ( password.value == password_repeat.value );
        
        if( !match ){           
            
            password_repeat.setErrors( {
                passwordsmismatch : true
            } );

        }
        
    }
    
}