import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {tap} from 'rxjs/operators';
import {Credentials, UserData, LoginData, SignupData} from '../../models/user';
import {AppSettingsService} from '../../../shared/services/app-settings.service';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json'
    })
};

const TYPE_TRAINER = 50;
const TYPE_CLIENT = 20;    

@Injectable({
  providedIn: 'root'
})
export class UserService {

    public credentials: Credentials;
    public data: UserData = null;
    constructor(
        private http: HttpClient,
        private appSettings: AppSettingsService
    ) {
        this.getCredentialsFromStorage();    
        this.getDataFromCookie();            
    }
    
    getAuthData (loginData: LoginData): Observable<Credentials> {        
        return this.http.post<Credentials>(`${this.appSettings.get('apiUrl')}/login`, loginData, httpOptions);        
    }
    
    create(data: SignupData): Observable<SignupData> {        
        return this.http.post<any>(`${this.appSettings.get('apiUrl')}/users`, data, httpOptions);        
    }
    
    getCredentialsFromStorage(): void {        
        if(!this.getCredentialsFromSession()) {
            this.getCredentialsFromCookie();
        }        
    }
    
    putCredentialsInCookie(): void {        
        localStorage.setItem('credentials', JSON.stringify(this.credentials));   
    }
    
    putCredentialsInSession(): void {        
        sessionStorage.setItem('credentials', JSON.stringify(this.credentials));        
    }
    
    getDataFromCookie(): void {        
        var data = <UserData>JSON.parse(localStorage.getItem('userdata'));
        
        if(data) {
            this.data = data;
        }       
    }
    
    putDataInCookie(): void {        
        localStorage.setItem('userdata', JSON.stringify(this.data));      
    }
    
    getCredentialsFromCookie(): boolean {        
        var credentials = <Credentials>JSON.parse(localStorage.getItem('credentials'));
        
        if(credentials){
            this.credentials = credentials;
            return true;
        }
        
        return false;     
   }
    
    getCredentialsFromSession(): boolean {        
        var credentials = JSON.parse (sessionStorage.getItem('credentials'));
        
        if(credentials) {
            this.credentials = credentials;
            return true;
        }
        
        return false;        
    }
    
    setCredentials(credentials: Credentials, remember: boolean = false): void {
        this.credentials = credentials;       
        
        if(remember) {      
            this.putCredentialsInCookie();
        } else {
            this.putCredentialsInSession();
        }        
   }
    
    setData(data: UserData): void {        
        this.data = data;
        this.putDataInCookie();        
   }
    
    getData(): Observable<UserData> {        
        return this.http.get<UserData>(`${this.appSettings.get('apiUrl')}/users/${this.credentials.uid}${this.getCredentialsQueryString()}`).pipe(
            tap()            
        );        
    }
    
    getCredentialsQueryString(): string {
        return '?uid=' + this.credentials.uid + '&token=' + this.credentials.token;
    }
        
    public isLogged(): boolean {
        return this.credentials ? true: false;
    }
    
    getId(): number
    {
        return this.data.id ? this.data.id : null; 
    }
    
    isTypeTrainer(): boolean
    {
        return this.data.type == TYPE_TRAINER ? true : false;
    }
    
    isTypeClient(): boolean
    {
        return this.data.type == TYPE_CLIENT ? true : false;
    }
    
    logout(): void {
        this.credentials = null;
        this.data = null;
        localStorage.remove('credentials');  
        localStorage.remove('userdata');        
        sessionStorage.clear();        
        window.location.href = '/user/guest/login';        
    }

}