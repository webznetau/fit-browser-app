//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {UserGuestScreenComponent} from './components/user-guest-screen/user-guest-screen.component';
import {UserLoginComponent} from './components/user-login/user-login.component';
import {UserSignupComponent} from './components/user-signup/user-signup.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {UserInvitationAcceptedComponent} from './components/user-invitation-accepted/user-invitation-accepted.component';
import {UserInvitationFailComponent} from './components/user-invitation-fail/user-invitation-fail.component';

//Guards
import {LoggedInRouteGuard} from '../shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from '../shared/guards/logged-out-route-guard';

const routes: Routes = [
    {
        path: 'guest',
        component: UserGuestScreenComponent,
        canActivate: [LoggedOutRouteGuard],
        children: [
            {
                path: '',
                redirectTo: 'login'
            },
            {
                path: 'login',
                component: UserLoginComponent,
            },
            {
                path: 'signup', 
                pathMatch: 'full',
                component: UserSignupComponent,
            }
        ]
    },
    {
        path: 'profile',
        component: UserProfileComponent,
        canActivate: [LoggedInRouteGuard],
    },
    {
        path: 'invitation-accepted',
        component: UserInvitationAcceptedComponent
    },
    {
        path: 'invitation-fail',
        component: UserInvitationFailComponent
    },
    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
