//Ng
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

//Material
import {MatTabsModule, MatCardModule, MatInputModule, MatIconModule, MatSlideToggleModule, MatProgressSpinnerModule, MatButtonModule} from '@angular/material';

//Components
import {UserGuestScreenComponent} from './components/user-guest-screen/user-guest-screen.component';
import {UserLoginComponent} from './components/user-login/user-login.component';
import {UserSignupComponent} from './components/user-signup/user-signup.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {UserInvitationAcceptedComponent} from './components/user-invitation-accepted/user-invitation-accepted.component';
import {UserInvitationFailComponent} from './components/user-invitation-fail/user-invitation-fail.component';

@NgModule({
  imports: [
    //Ng
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    //Material
    MatTabsModule,
    MatInputModule,
    MatCardModule,
    MatIconModule,
    MatSlideToggleModule,
    MatProgressSpinnerModule,
    MatButtonModule
  ],
  declarations: [UserGuestScreenComponent, UserLoginComponent, UserSignupComponent, UserProfileComponent, UserInvitationAcceptedComponent, UserInvitationFailComponent]
})
export class UserModule {}
